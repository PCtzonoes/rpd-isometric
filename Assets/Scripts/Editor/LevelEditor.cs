using System;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using static Editor.TileEditorColor;

namespace Editor
{
	public class LevelEditor : EditorWindow
	{
		[SerializeField] private float tileSize = 20f;
		[SerializeField] private float leftPadding = 5f;
		[SerializeField] private float tilePadding = 3f;
		[SerializeField] private float gridStart = 160f;

		private PieceType _selectedTile = PieceType.NoPiece;
		private readonly LevelData _levelData = new LevelData();

		private static GameObject _tilePrefabGO;
		private static GameObject _blockerPiecePrefabGO;
		private static GameObject _startingPieceGO;
		private static GameObject _pathPieceGO;
		private static GameObject _objectivePieceGO;
		private static GameObject _directionPieceGO;
		private static GameObject _collectPieceGO;
		private static GameObject _defaultPieceGO;
		private static GameObject _playerGo;

		private void OnGUI()
		{
			GUILayout.Label("Level Editor");

			GUILayout.BeginHorizontal();
			_levelData.Width = EditorGUILayout.IntField(_levelData.Width);
			_levelData.Height = EditorGUILayout.IntField(_levelData.Height);
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();

			_levelData.Height = EditorGUILayout.IntField(_levelData.Height);
			GUILayout.EndHorizontal();

			if (GUILayout.Button("Create Grid"))
			{
				CreateGrid();
			}
			// myData.Grid = MazeGenerator.GenerateV2(new IntVector2(myData.Width, myData.Height), 4, 4);

			if (GUILayout.Button("Save Level")) SaveLevel();

			var e = Event.current;


			for (var i = 0; i < TileColors.Keys.Count; i++)
			{
				var colorArray = new Rect
				(
					(tileSize + tilePadding * 2) * i + leftPadding,
					gridStart - 2 * tileSize,
					tileSize, tileSize
				);

				if (e.type == EventType.MouseDown && colorArray.Contains(e.mousePosition)) _selectedTile = (PieceType)i;

				var selectedTileRect = new Rect
				(
					(tileSize + tilePadding * 2) * (int)_selectedTile + leftPadding + 0.25f,
					gridStart - 2 * tileSize + 0.25f,
					tileSize * 0.5f, tileSize * 0.5f
				);
				EditorGUI.DrawRect(selectedTileRect, Color.black);

				EditorGUI.DrawRect(colorArray, PieceColor((PieceType)i));
			}

			if (_levelData.Grid == null) return;

			for (var i = _levelData.Grid.GetLength(0) - 1; i >= 0; i--)
			for (var j = _levelData.Grid.GetLength(1) - 1; j >= 0; j--)
			{
				var gridTile = new Rect
				(
					(tileSize + tilePadding) * i + leftPadding,
					(tileSize + tilePadding) * j + gridStart,
					tileSize, tileSize
				);

				if ((e.type == EventType.MouseDown || e.type == EventType.MouseDrag) &&
				    gridTile.Contains(e.mousePosition))
				{
					_levelData.Grid[i, j] = _selectedTile;
					Repaint();
				}

				var objectIndex = _levelData.Grid[i, j];
				EditorGUI.DrawRect(gridTile, TileColors[objectIndex]);
			}
		}

		[MenuItem("Window/LevelEditor")]
		private static void ShowWindow()
		{
			var window = GetWindow<LevelEditor>();
			window.titleContent = new GUIContent("Level");
			window.Show();
			_tilePrefabGO = Resources.Load<GameObject>("Tile");
			_blockerPiecePrefabGO = Resources.Load<GameObject>("PathSinglePiece/BlockerPiece");
			_collectPieceGO = Resources.Load<GameObject>("PathSinglePiece/CollectPiece");
			_defaultPieceGO = Resources.Load<GameObject>("PathSinglePiece/PathPiece");
			_directionPieceGO = Resources.Load<GameObject>("PathSinglePiece/DirectionPiece");
			_objectivePieceGO = Resources.Load<GameObject>("PathSinglePiece/ObjectivePiece");
			_pathPieceGO = Resources.Load<GameObject>("PathSinglePiece/PathPiece");
			_startingPieceGO = Resources.Load<GameObject>("PathSinglePiece/StartingPiece");
			_playerGo = Resources.Load<GameObject>("Player");
		}

		private void CreateGrid()
		{
			_levelData.Grid = new PieceType[_levelData.Height, _levelData.Width];
		}

		private void SaveLevel()
		{
			for (var x = 0; x < _levelData.Height; x++)
			for (var y = 0; y < _levelData.Width; y++)
			{
				var spawnedTile = Instantiate(_tilePrefabGO, new Vector3(x, 0, y), Quaternion.identity)
					.GetComponent<Tile>();
				spawnedTile.name = $"Tile {x} {y}";

				var isOffset = x % 2 == 0 && y % 2 != 0 || x % 2 != 0 && y % 2 == 0;
				spawnedTile.isOffset = isOffset;
				spawnedTile.gridPosition = new Vector2Int(x, y);
				switch (_levelData.Grid[x, y])
				{
					case PieceType.NoPiece:
						break;
					case PieceType.ChangeDirection:
						var piece = Instantiate(_directionPieceGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(piece);
						break;
					case PieceType.Block:
						var bPiece = Instantiate(_blockerPiecePrefabGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(bPiece);
						break;
					case PieceType.Start:
						var sPiece = Instantiate(_startingPieceGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(sPiece);
						Instantiate(_playerGo, new Vector3(x, 0, y), Quaternion.identity);
						break;
					case PieceType.Objective:
						var oPiece = Instantiate(_collectPieceGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(oPiece);
						break;
					case PieceType.DefaultPiece:
						var dPiece = Instantiate(_defaultPieceGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(dPiece);
						break;
					case PieceType.Finish:
						var fPiece = Instantiate(_objectivePieceGO,
							new Vector3(x, 0, y), Quaternion.identity).GetComponent<PathPiece>();
						spawnedTile.AddPiece(fPiece);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}
	}

	[Serializable]
	internal class LevelData
	{
		public int Width { get; set; } = 10;
		public int Height { get; set; } = 10;
		public PieceType[,] Grid { get; set; }
	}
}