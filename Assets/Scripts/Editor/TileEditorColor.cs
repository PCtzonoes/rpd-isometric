using System.Collections.Generic;
using UnityEngine;

namespace Editor
{
	internal static class TileEditorColor
	{
		public enum PieceType : int
		{
			NoPiece,
			ChangeDirection,
			Block,
			Start,
			Objective,
			Finish,
			DefaultPiece,
		}

		internal static readonly Dictionary<PieceType, Color> TileColors = new Dictionary<PieceType, Color>()
		{
			{ PieceType.NoPiece, Color.green },
			{ PieceType.Start, Color.white },
			{ PieceType.Block, Color.red },
			{ PieceType.ChangeDirection, new Color(153,153,0,255) },
			{ PieceType.Finish, Color.cyan },
			{ PieceType.Objective, Color.magenta },
			{ PieceType.DefaultPiece, Color.blue },
		};

		public static Color PieceColor(PieceType piece)
		{
			return TileColors[piece];
		}
	}
}