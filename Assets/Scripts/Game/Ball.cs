using System;
using Helper;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
	[SerializeField] private GameEvent onTurn;
	[Header("Movement")] [SerializeField] private Vector3 direction = Vector3.forward;
	[SerializeField] private float moveSpeed = 2.5f;
	[SerializeField] private Vector3 offSetY;
	[SerializeField] private LayerMask tileLayer = 1 << 3;

	private bool _levelStarted = false;

	private void Awake()
	{
		transform.position += offSetY;
		var lookAt = transform.position + direction;
		transform.LookAt(lookAt);
	}

	private void Update()
	{
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.A))
		{
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			SceneManager.LoadScene(2, LoadSceneMode.Single);
			UIScreenController.ShowScreen<GameplayUIScreen>();
		}
#endif // UNITY_EDITOR

		if (!_levelStarted) return;

		transform.position += new Vector3(direction.x, 0, direction.z) * (Time.deltaTime * moveSpeed);
		CheckGround();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!_levelStarted) return;
		var pathPiece = other.GetComponent<PathPiece>();
		if (!pathPiece) return;
		switch (pathPiece.Behavior)
		{
			case PathPiece.PieceType.Normal:
				Debug.Log($"Ball Reached {pathPiece.name}");
				break;
			case PathPiece.PieceType.ChangeDirection:
				transform.position = pathPiece.transform.position;
				transform.position = new Vector3(pathPiece.transform.position.x + offSetY.x,
					offSetY.y,
					pathPiece.transform.position.z + offSetY.z);
				var dir = pathPiece.Direction();
				if (dir != Vector3.zero)
				{
					onTurn?.Invoke();
					direction = dir;
					var lookAt = transform.position + direction;
					transform.LookAt(lookAt);
				}

				break;
			case PathPiece.PieceType.Block:
				GameOver(false, "Octopus hits block piece");
				break;
			case PathPiece.PieceType.Objective:
				if (LevelObjective.Objectives.Count == 0) GameOver(true);
				else GameOver(false, "Missing Stars");
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	private static void GameOver(bool victory, string msg = null)
	{
		Time.timeScale = 0;
		var gameOverUi = UI.UIScreenController.LoadScreen<UI.GameOverUiScreen>();
		if (victory) LevelProgression.LevelUnlocked(SceneManager.GetActiveScene().buildIndex + 1);
		gameOverUi.Victory(victory, msg);
	}

	public void InitializeMovement()
	{
		_levelStarted = true;
		var lookAt = transform.position + direction;
		transform.LookAt(lookAt);
	}

	private void CheckGround()
	{
		var ray = new Ray(transform.position, Vector3.down);
		if (Physics.Raycast(ray, out var hit, 1000, tileLayer))
		{
			var tile = hit.transform.GetComponent<Tile>();
			if (!tile.PlayerWalkable)
			{
				GameOver(false, "Octopus falls down");
				_levelStarted = false;
			}
		}
		else
		{
			Debug.Log("Player out off Grid");
			_levelStarted = false;
			GameOver(false, "Octopus got lost in space");
		}
	}
}