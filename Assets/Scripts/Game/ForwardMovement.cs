using System;
using UnityEngine;
using UnityEngine.Serialization;
public class ForwardMovement : MonoBehaviour
{
	[Range(0, 15f), SerializeField] private float moveSpeed = 5f;

	private void Update()
	{
		transform.position += moveSpeed * Time.deltaTime * transform.forward;
	}
}