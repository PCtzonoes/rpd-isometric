using UnityEngine;

public class BlockedTilePiece : TilePieceBehavior
{
	public override void AddPiece(in PathPiece toAdd)
	{
		Debug.LogError($"Should not add Piece to this tile: {gameObject.name}");
	}

	public override bool CanPlacePiece()
	{
		return false;
	}
	
	public override bool PlayerWalkable()
	{
		return false;
	}
}