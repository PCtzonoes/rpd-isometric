using NaughtyAttributes;
using UnityEngine;
using Template;

public class GridManager : MonoBehaviour
{
	[SerializeField] private PathPieces[] prefabs;
	[SerializeField] private bool instateateGrid = false;
	[SerializeField] private int width = 10;
	[SerializeField] private int height = 10;
	[SerializeField] private Tile tilePrefab;
	[SerializeField] private PathPiece pathPiecePrefab;
	[SerializeField] private PathPiece objectivePrefab;
	[SerializeField] private Ball playerPrefab;
	[SerializeField] private LayerMask tileLayer;
	[SerializeField] private Vector2Int startPosition = Vector2Int.zero;
	[SerializeField] private Vector2Int targetPosition = Vector2Int.one;
	[Range(0.2f, 5)] [SerializeField] private float piecesOffset = 1f;

	private GridInstance<Tile> _gridInstance;
	private Tile _currentTile = null;
	private PathPieces _currentSelectedPieces = null;
	private Camera _camera;
	private bool _canPlace = false;
	private bool _levelStarted = false;
	private int _lastSelectedPiece = 0;

	private void Awake()
	{
		_camera = Camera.main;
		if (instateateGrid)
		{
			GenerateGrid();
			_currentTile = _gridInstance.GetItem(new Vector2Int(0, 0));
		}
		else
		{
			_gridInstance = new GridInstance<Tile>(new Vector2Int(width, height));
		}

		UI.UIScreenController.AddData<UI.GameplayUIScreen, GridManager>(this);
	}

	[Button("Instantiate Grid")]
	private void GenerateGrid()
	{
		_gridInstance = new Template.GridInstance<Tile>(new Vector2Int(width, height));
		for (var x = 0; x < width; x++)
		for (var y = 0; y < height; y++)
		{
			var spawnedTile = Instantiate(tilePrefab, new Vector3(x, 0, y), Quaternion.identity);
			spawnedTile.name = $"Tile {x} {y}";

			var isOffset = x % 2 == 0 && y % 2 != 0 || x % 2 != 0 && y % 2 == 0;
			spawnedTile.isOffset = isOffset;
			spawnedTile.gridPosition = new Vector2Int(x, y);

			_gridInstance.SetItem(x, y, spawnedTile);
		}

		var startTile = _gridInstance.GetItem(startPosition);
		var targetTile = _gridInstance.GetItem(targetPosition);
		var startPiece = Instantiate(pathPiecePrefab, startTile.transform.position, Quaternion.identity);
		startTile.AddPiece(startPiece);
		var objectivePiece = Instantiate(objectivePrefab, targetTile.transform.position, Quaternion.identity);
		targetTile.AddPiece(objectivePiece);
		Instantiate(playerPrefab, startTile.transform.position, Quaternion.identity);
	}

	private void Update()
	{
		if (_levelStarted) return;

		if (Input.GetMouseButtonDown(0) && _canPlace)
		{
			Ray ray2 = _camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit2;
			if (Physics.Raycast(ray2, out hit2, 100, tileLayer))
			{
				if (_currentTile == hit2.collider.gameObject.GetComponent<Tile>()) PlaceCurrentPieces();
				else
				{
					Destroy(_currentSelectedPieces.gameObject);
					_currentSelectedPieces = null;
				}
			}
			else
			{
				Destroy(_currentSelectedPieces.gameObject);
				_currentSelectedPieces = null;
			}
		}

		if (Input.GetKeyDown(KeyCode.W))
		{
			SelectPieces((++_lastSelectedPiece) % prefabs.Length);
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			_lastSelectedPiece--;
			if (_lastSelectedPiece < 0) SelectPieces(prefabs.Length - 1);
			else SelectPieces(_lastSelectedPiece);
		}


		foreach (var tile in _gridInstance.Grid)
		{
			tile.RemoveHighlight();
		}

		Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 100, tileLayer))
		{
			_currentTile = hit.collider.gameObject.GetComponent<Tile>();
		}

		if (_currentTile == null) return;

		if (_currentSelectedPieces == null) return;

		_currentSelectedPieces.transform.position = _currentTile.transform.position + Vector3.up * piecesOffset;
		var valid = _currentSelectedPieces.ValidEmplacement(out var tiles);
		_canPlace = valid;
		foreach (var tile in tiles)
		{
			tile.Highlight(valid);
		}
	}

	private void PlaceCurrentPieces()
	{
		_canPlace = false;
		_currentSelectedPieces.SetPieces();
		_currentSelectedPieces = null;
	}

	public void SelectPieces(int prefabIndex)
	{
		if (prefabIndex >= prefabs.Length)
		{
			Debug.LogError($"Selected Pieces Overflows prefabs array.");
			return;
		}

		if (_currentSelectedPieces != null)
		{
			Destroy(_currentSelectedPieces.gameObject);
		}

		_currentSelectedPieces = Instantiate(prefabs[prefabIndex],
			_currentTile.transform.position + Vector3.up * piecesOffset,
			Quaternion.identity);
		_lastSelectedPiece = prefabIndex;
	}

	public void StartGame()
	{
		_levelStarted = true;
	}

	public void AddTile(Tile tile, Vector2Int gridPosition)
	{
		_gridInstance.SetItem(gridPosition, tile);
		_currentTile = tile;
	}
}