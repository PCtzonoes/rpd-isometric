using System.Collections.Generic;
using UnityEngine;

public class MultiPieceTilePiece : TilePieceBehavior
{
	[SerializeField] private List<PathPiece> pieces = new List<PathPiece>(3);

	public override void AddPiece(in PathPiece toAdd)
	{
		pieces.Add(toAdd);
	}

	public override bool CanPlacePiece()
	{
		return pieces.Count < pieces.Capacity;
	}
	
	public override bool PlayerWalkable()
	{
		return pieces[0] != null && pieces[0].Behavior != PathPiece.PieceType.Block;
	}
}