using UnityEngine;

public class SinglePieceTilePiece : TilePieceBehavior
{
	[SerializeField] private PathPiece piece;

	public override void AddPiece(in PathPiece toAdd)
	{
		piece = toAdd;
	}

	public override bool CanPlacePiece()
	{
		return piece == null;
	}

	public override bool PlayerWalkable()
	{
		return piece != null && piece.Behavior != PathPiece.PieceType.Block;
	}
}