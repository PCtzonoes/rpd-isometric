using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(TilePieceBehavior))]
public class Tile : MonoBehaviour
{
	private static readonly Vector2Int InvalidPosition = new Vector2Int(-1, -1);

	public TilePieceBehavior pieceBehavior;
	[SerializeField] private new MeshRenderer renderer; //hiding obsolete property

	[SerializeField] private Material baseColor;
	[SerializeField] private Material offsetColor;
	[SerializeField] private Material highlightValidMaterial;
	[SerializeField] private Material highlightInvalidMaterial;

	[HideInInspector] public bool isOffset = false;
	public bool IsHighlight { get; private set; } = false;
	public Vector2Int gridPosition = InvalidPosition;
	public bool CanPlacePiece => pieceBehavior.CanPlacePiece();

	public bool PlayerWalkable => pieceBehavior.PlayerWalkable();

	private void Start()
	{
		if (renderer == null) renderer = GetComponent<MeshRenderer>();
		renderer.material = GridMaterial();
		
		if (gridPosition == InvalidPosition) return;

		var man = FindObjectOfType<GridManager>();
		man.AddTile(this, gridPosition);
	}

	private Material GridMaterial()
	{
		return isOffset ? offsetColor : baseColor;
	}

	public void Highlight(bool isValid)
	{
		IsHighlight = true;
		renderer.material = (isValid) ? highlightValidMaterial : highlightInvalidMaterial;
	}

	public void RemoveHighlight()
	{
		IsHighlight = false;
		renderer.material = GridMaterial();
	}

	public void AddPiece(PathPiece toAdd)
	{
		pieceBehavior.AddPiece(toAdd);
	}

	public void Init(bool isOffset, Vector2Int position)
	{
		this.isOffset = isOffset;
		gridPosition = position;
	}
}