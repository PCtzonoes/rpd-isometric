using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class TilePieceBehavior : MonoBehaviour
{
	public abstract void AddPiece(in PathPiece toAdd);
	public abstract bool CanPlacePiece();
	public abstract bool PlayerWalkable();
}