using System;
using System.Collections.Generic;
using Helper;
using Unity.VisualScripting;
using UnityEngine;


public class LevelObjective : MonoBehaviour
{
	[SerializeField] private GameEvent onPickUp;
	public static HashSet<LevelObjective> Objectives { get; private set; } = new HashSet<LevelObjective>();

	private void OnTriggerEnter(Collider other)
	{
		var player = other.GetComponent<Ball>();
		if (!player) return;

		onPickUp.Invoke();
		gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		Objectives.Add(this);
	}

	private void OnDisable()
	{
		Objectives.Remove(this);
	}
}