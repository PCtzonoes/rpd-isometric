using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "UnlockedLevels", menuName = "Game/LevelProgression", order = 0)]
public class LevelProgression : ScriptableObject
{
	private readonly Dictionary<int, bool> _levels = new Dictionary<int, bool>();

	[SerializeField] private int sceneCount = 25;
	[SerializeField] private int[] unlockedLevels = new[] { 2, 5, 15 };

	private static List<LevelProgression> _lazyFix = new List<LevelProgression>(1);
	

	public void Reset()
	{
		for (int i = 2; i < sceneCount; i++)
		{
			_levels.Add(i, false);
		}

		foreach (var level in unlockedLevels)
		{
			if (_levels.ContainsKey(level))
				_levels[level] = true;
			else Debug.LogError($"No Level {level} in build settings");
		}
		_lazyFix.Add(this);
	}

	public void OnVictory(int levelUnlocked)
	{
		if (_levels.ContainsKey(levelUnlocked))
			_levels[levelUnlocked] = true;
		else Debug.LogError("No Level in build settings");
	}

	public static void LevelUnlocked(int buildIndex)
	{
		foreach (var progress in _lazyFix)
		{
			if (!progress._levels.ContainsKey(buildIndex)) continue;
			progress._levels[buildIndex] = true;
		}
	}

	public bool ValidLevel(int buildIndex)
	{
		return _levels.ContainsKey(buildIndex) && _levels[buildIndex];
	}

	public int[] UnlockedLevels()
	{
		return (from level in _levels where level.Value select level.Key).ToArray();
	}
}