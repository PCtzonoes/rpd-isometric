using System;
using Helper;
using UnityEngine;
using UnityEngine.Events;


public class OnStart : MonoBehaviour
{
	[SerializeField] private GameEvent onStart;

	private void Start()
	{
		onStart.Invoke();
	}
}