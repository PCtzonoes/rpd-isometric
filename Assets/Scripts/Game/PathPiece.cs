using UnityEngine;

public class PathPiece : MonoBehaviour
{
	public enum PieceType
	{
		Normal,
		ChangeDirection,
		Block,
		Objective
	}

	[SerializeField] private PieceType behavior = PieceType.Normal;
	[SerializeField] private Transform direction;
	private bool triggerOnce = true;

	public PieceType Behavior => behavior;

	public Vector3 Direction()
	{
		if (!triggerOnce) return Vector3.zero;

		triggerOnce = false;
		return direction.forward;
	}
}