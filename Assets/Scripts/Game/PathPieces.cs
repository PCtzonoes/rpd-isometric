using System;
using System.Collections.Generic;
using Helper;
using Unity.Mathematics;
using UnityEngine;


public class PathPieces : MonoBehaviour
{
	[SerializeField] private GameEvent onPlacement;
	public PathPiece[] pieces;
	[SerializeField] private LayerMask tileLayer;
	[SerializeField] private Vector3 offSet;

	public bool ValidEmplacement(out List<Tile> tiles)
	{
		tiles = new List<Tile>(pieces.Length);
		foreach (var piece in pieces)
		{
			Ray ray = new Ray(piece.transform.position + offSet, Vector3.down);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100, tileLayer))
			{
				var tile = hit.transform.GetComponent<Tile>();
				tiles.Add(tile);
				if (!tile.CanPlacePiece) return false;
			}
			else return false;
		}

		return true;
	}

	public void SetPieces()
	{
		foreach (var piece in pieces)
		{
			Ray ray = new Ray(piece.transform.position + offSet, Vector3.down);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100, tileLayer))
			{
				var tile = hit.transform.GetComponent<Tile>();
				var newPiece = Instantiate(piece, tile.transform.position, transform.rotation);
				tile.AddPiece(newPiece);
				// var material = newPiece.GetComponent<Renderer>().material;
				// var tColor = material.color;
				// tColor.a = 245;
				// material.color = tColor;
			}
		}
		onPlacement?.Invoke();
		Destroy(gameObject);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Q))
		{
			transform.eulerAngles += Vector3.up * 90;
			
		}

		if (Input.GetKeyDown(KeyCode.E))
		{
			transform.eulerAngles -= Vector3.up * 90;
		}
	}
}