using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class RotateSky : MonoBehaviour
{
	[Range(0, 5)] [SerializeField] private float rotateSpeed = 1.2f;

	private static readonly int Rotation = Shader.PropertyToID("_Rotation");

	private void Update()
	{
		RenderSettings.skybox.SetFloat(Rotation, Time.time * rotateSpeed);
	}
}