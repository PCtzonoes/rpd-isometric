using System.Collections.Generic;
using UnityEngine;

namespace Template
{
	public struct GridInstance<T>
	{
		public GridInstance(T[,] grid)
		{
			this.Grid = grid;
		}

		public GridInstance(Vector2Int size)
		{
			Grid = new T[size.x, size.y];
		}

		public T[,] Grid { get; private set; }

		public void Initialize(Vector2Int size, T item)
		{
			Grid = new T[size.x, size.y];
			for (var i = 0; i < Grid.GetLength(0); i++)
			for (var j = 0; j < Grid.GetLength(1); j++)
				Grid[i, j] = item;
		}

		public readonly T GetItem(Vector2Int pos)
		{
			return Grid[pos.x, pos.y];
		}

		public void SetItem(int posX, int posY, T item)
		{
			Grid[posX, posY] = item;
		}

		public void SetItem(Vector2Int pos, T item)
		{
			Grid[pos.x, pos.y] = item;
		}

		public readonly bool InBounds(Vector2Int id)
		{
			return 0 <= id.x && id.x < Grid.GetLength(0)
			                 && 0 <= id.y && id.y < Grid.GetLength(1);
		}

		public readonly IEnumerable<T> AllNeighbors(Vector2Int targetNode, int distance = 1)
		{
			foreach (var dir in Extensions.AllDirections)
			{
				var neighborNode = targetNode + dir * distance;
				if (!InBounds(neighborNode)) continue;
				var neighbor = Grid[neighborNode.x, neighborNode.y];
				{
					yield return neighbor;
				}
			}
		}

		public readonly IEnumerable<T> CardinalNeighbors(Vector2Int targetNode, int distance = 1)
		{
			foreach (var dir in Extensions.CardinalDirections)
			{
				var neighborNode = targetNode + dir * distance;
				if (!InBounds(neighborNode)) continue;
				var neighbor = Grid[neighborNode.x, neighborNode.y];
				{
					yield return neighbor;
				}
			}
		}

		public IEnumerable<T> DiagonalNeighbors(Vector2Int targetNode, int distance = 1)
		{
			foreach (var dir in Extensions.DiagonalDirections)
			{
				var neighborNode = targetNode + dir * distance;
				if (!InBounds(neighborNode)) continue;
				var neighbor = Grid[neighborNode.x, neighborNode.y];
				{
					yield return neighbor;
				}
			}
		}

		public readonly IEnumerable<T> PerpendicularNeighbours(
			Vector2Int targetNode, Vector2Int direction, int distance = 1)
		{
			var directions = direction == Vector2Int.up || direction == Vector2Int.down
				? Extensions.RightLeft
				: Extensions.UpDown;

			foreach (var dir in directions)
			{
				var neighborNode = targetNode + dir * distance;
				if (!InBounds(neighborNode)) continue;
				var neighbor = Grid[neighborNode.x, neighborNode.y];
				{
					yield return neighbor;
				}
			}
		}
	}

	public static class Extensions
	{
		public static readonly Vector2Int DownRight = new Vector2Int(1, -1);
		public static readonly Vector2Int DownLeft = new Vector2Int(-1, -1);
		public static readonly Vector2Int UpLeft = new Vector2Int(-1, 1);
		public static readonly Vector2Int UpRight = new Vector2Int(1, 1);

		public static readonly Vector2Int[] AllDirections =
		{
			Vector2Int.down, Vector2Int.up, Vector2Int.left, Vector2Int.right,
			DownRight, DownLeft, UpRight, UpLeft
		};

		public static readonly Vector2Int[] CardinalDirections =
		{
			Vector2Int.down, Vector2Int.up, Vector2Int.left, Vector2Int.right
		};

		public static readonly Vector2Int[] DiagonalDirections =
		{
			DownRight, DownLeft, UpRight, UpLeft
		};

		public static readonly Vector2Int[] UpDown =
		{
			Vector2Int.up, Vector2Int.down
		};

		public static readonly Vector2Int[] RightLeft =
		{
			Vector2Int.right, Vector2Int.left
		};
	}
}