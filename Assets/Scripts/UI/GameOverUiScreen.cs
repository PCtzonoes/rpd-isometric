using Helper;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class GameOverUiScreen : UIScreen
	{
		[SerializeField] private GameEvent onVictory;
		[SerializeField] private GameEvent onLose;
		[SerializeField] private TMP_Text tx;

		public void Victory(bool success, string loseMsg = null)
		{
			if (success)
			{
				tx.text = "Victory";
				onVictory.Invoke();
			}
			else
			{
				tx.text = loseMsg == null ? "Failed" : $"Failed\n{loseMsg}";
				onLose.Invoke();
			}
		}

		public void MenuButton()
		{
			SceneManager.LoadScene(1, LoadSceneMode.Single);
			UIScreenController.ShowScreen<MainMenuUIScreen>();
		}

		public void RetryButton()
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
			var s = UIScreenController.LoadScreen<GameplayUIScreen>();
			s.ResetButton();
		}

		public void NextLevelButton()
		{
			var nextLvl = SceneManager.GetActiveScene().buildIndex + 1;
			SceneManager.LoadScene(nextLvl, LoadSceneMode.Single);
			var s = UIScreenController.LoadScreen<GameplayUIScreen>();
			s.ResetLevel(nextLvl);
		}
	}
}