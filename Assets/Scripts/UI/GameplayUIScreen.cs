using Helper;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class GameplayUIScreen : UIScreen, IDataReceiver<GridManager>
	{
		[SerializeField] private GameEvent onGameStarted;
		[SerializeField] private GameObject[] onEnable;
		[SerializeField] private TMP_Text levelName;
		private GridManager _gridManager;

		protected override void OnEnable()
		{
			base.OnEnable();
			foreach (var go in onEnable)
			{
				go.SetActive(true);
			}
		}

		public void SetData(GridManager item)
		{
			_gridManager = item;
			var text = SceneManager.GetActiveScene().name;
			levelName.text = text.Replace('_', ' ');
		}

		public void SelectionButton(int index)
		{
			_gridManager.SelectPieces(index);
		}

		public void StartButton()
		{
			_gridManager.StartGame();
			onGameStarted.Invoke();
		}

		public void ResetButton()
		{
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);

			UIScreenController.ShowScreen<GameplayUIScreen>();
		}

		public void ResetLevel(int nextLvl)
		{
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			SceneManager.LoadScene(nextLvl, LoadSceneMode.Single);
			UIScreenController.ShowScreen<GameplayUIScreen>();
		}
	}
}