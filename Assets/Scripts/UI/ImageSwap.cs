using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI
{
	public class ImageSwap : MonoBehaviour
	{
		private static List<ImageSwap> _swaps = new List<ImageSwap>();
		public int number = 0;
		[SerializeField] private GameObject falseGo;
		[SerializeField] private GameObject trueGo;

		private void OnEnable()
		{
			_swaps.Add(this);
		}

		private void OnDisable()
		{
			_swaps.Remove(this);
		}

		public static void Swap(in int[] numbers)
		{
			foreach (var s in _swaps)
			{
				if (!numbers.Contains(s.number)) continue;
				// Debug.Log("Swapping");
				s.trueGo.SetActive(true);
				s.falseGo.SetActive(false);
			}
		}
	}
}