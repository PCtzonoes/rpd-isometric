using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class SceneSelection : UIScreen
	{
		[SerializeField] private LevelProgression levelProg;
		
		private void Awake()
		{
			levelProg.Reset();
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			ImageSwap.Swap(levelProg.UnlockedLevels());
		}

		public void SelectionLevel(int levelBuildIndex)
		{
			Debug.Log($"level selected: {levelBuildIndex}");
			if (!levelProg.ValidLevel(levelBuildIndex))
			{
				Debug.Log($"level Blocked");
				return;
			}

			SceneManager.LoadScene(levelBuildIndex, LoadSceneMode.Single);
			UIScreenController.ShowScreen<GameplayUIScreen>();
		}

		public void MenuButton()
		{
			UIScreenController.ShowScreen<MainMenuUIScreen>();
			SceneManager.LoadScene(1, LoadSceneMode.Single);
		}

		private void Update()
		{
			ImageSwap.Swap(levelProg.UnlockedLevels());
		}
	}
}