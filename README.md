# Pathline

---

## Theme | Setting | Genre

Pathline is a top view 3d puzzle game that the player helps the baby octopus in space to reach her home. The player must
make a path with various cubic shapes to help the baby octopus.
---

## Hook

Pathline is the first puzzle game that uses a grid board and shape placement mechanic at the same time. The combination
of these features encourages the player to think outside of the box and overcome challenges.
---

## Goals

The design goal of this game is to produce a fun puzzle mechanic that can generate many levels. The focus of the
production is to create a pipeline for puzzle levels. Having a pipeline for puzzle levels will allow the team to make
many levels in a brief period.
---

## Project Scope

### Game Time Scale

	Four week of production time is given to the project

### Team Size

	Three members will be contributing to the project.  

---

## Game Page

[Pathline Itch.io Page](https://aakashbhagchandani.itch.io/pathline)

---

## Team Members

[Pedro Augusto de Almeida Soares](https://gitlab.com/PCtzonoes)

[Oguz Bicer](https://github.com/Oguzz-Stu)

Aakash Kumar Bhagchandani



